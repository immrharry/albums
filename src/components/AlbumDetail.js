import React from "react";
import { View, Text, StyleSheet, Image, Linking } from "react-native";
import { Button } from "react-native-elements";
import Card from "./Card";
import CardSection from "./CardSection";
const AlbumDetail = ({ album }) => {
  const { title, thumbnail_image, artist, image, url } = album;
  return (
    <Card>
      <CardSection>
        <View style={styles.thumbnailImageStyle}>
          <Image
            source={{ uri: thumbnail_image }}
            style={styles.thumbnailStyle}
          />
        </View>
        <View style={styles.cardSectionTextStyle}>
          <Text style={styles.cardSectionTitleStyle}>{title}</Text>
          <Text>{artist}</Text>
        </View>
      </CardSection>
      <CardSection>
        <Image source={{ uri: image }} style={styles.cardSectionImageStyle} />
      </CardSection>
      <CardSection>
        <View style={{ flex: 1 }}>
          <Button
            title="Buy Now"
            type="outline"
            onPress={() => Linking.openURL(url)}
          />
        </View>
      </CardSection>
    </Card>
  );
};

const styles = StyleSheet.create({
  cardSectionTextStyle: {
    flexDirection: "column",
    justifyContent: "space-around"
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailImageStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10
  },
  cardSectionTitleStyle: {
    fontSize: 18
  },
  cardSectionImageStyle: {
    height: 300,
    flex: 1,
    width: null
  },
  cardSectionButtonStyle: {
    flex: 1,
    alignSelf: "center",
    marginLeft: 5,
    marginRight: 5
  }
});

export default AlbumDetail;
