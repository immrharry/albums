/*//import { createStackNavigator, createAppContainer } from "react-navigation";
import { createAppContainer } from "react-navigation";
import React from "react";
import { AppRegistry } from "react-native";
import Header from "./src/components/Header";

/*const navigator = createStackNavigator(
  {
    Home: HomeScreen
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      title: "Albums",

      headerStyle: {
        backgroundColor: "#f8f8f8",
        height: 60,
        paddingTop: 40,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        elevation: 9
      },

      headerTitleStyle: {
        textAlign: "center",
        flex: 1,
        fontSize: 25,
        paddingBottom: 20
      }
    }
  }
);

export default createAppContainer(navigator);*/

/*const App = () => {
  return <Header />;
};

export default createAppContainer(App);*/

import "react-native-gesture-handler";
import * as React from "react";
import { View } from "react-native";
import Header from "./src/components/Header";
import AlbumList from "./src/components/AlbumList";
import { NavigationContainer } from "@react-navigation/native";

export default App = () => {
  return (
    <View style={{ flex: 1 }}>
      <Header headerText="Albums" />
      <AlbumList />
    </View>
  );
};
